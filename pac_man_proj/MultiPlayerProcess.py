from network import Network
from time import sleep
from PyQt5.QtGui import QGuiApplication


def do_multiplayer(network_id, player1_x, player1_y, player1_head_direction, lives_player1, player2_x, player2_y,
                   player2_head_direction, lives_player2, green_x, green_y, red_x, red_y, pink_x,
                   pink_y, yellow_x, yellow_y, special_power_x, special_power_y, ghosts_can_be_eaten,
                   is_eaten_green, is_eaten_red, is_eaten_pink, is_eaten_yellow, is_eaten_player1, is_eaten_player2, wait_signal, num_of_players):

    network = Network()
    network_id.value = int(network.id)
    if network_id.value == 1:
        player1_x.value = 20.0
        player1_y.value = 20.0
    run = True
    while run:
        if network_id.value == 0:
            player2_x.value, player2_y.value, player2_head_direction.value, \
            lives_player2.value = parse_data(send_data(network, network_id,
                                                       player1_x, player1_y,
                                                       player1_head_direction,
                                                       lives_player1,
                                                       green_x, green_y,
                                                       red_x, red_y, pink_x,
                                                       pink_y, yellow_x,
                                                       yellow_y,
                                                       special_power_x,
                                                       special_power_y,
                                                       ghosts_can_be_eaten,
                                                       is_eaten_green,
                                                       is_eaten_red,
                                                       is_eaten_pink,
                                                       is_eaten_yellow,
                                                       is_eaten_player1,
                                                       is_eaten_player2, wait_signal, num_of_players),
                                                       network_id)
        elif network_id.value == 1:
            player2_x.value, player2_y.value, player2_head_direction.value, \
            lives_player2.value, green_x.value, green_y.value, \
            red_x.value, red_y.value, \
            pink_x.value, pink_y.value, \
            yellow_x.value, yellow_y.value, \
            special_power_x.value, special_power_y.value, \
            ghosts_can_be_eaten.value, is_eaten_green.value, \
            is_eaten_red.value, is_eaten_pink.value, \
            is_eaten_yellow.value, is_eaten_player1.value,\
            is_eaten_player2.value = parse_data(send_data(network, network_id,
                                                          player1_x, player1_y,
                                                          player1_head_direction,
                                                          lives_player1,
                                                          green_x, green_y,
                                                          red_x, red_y, pink_x,
                                                          pink_y, yellow_x,
                                                          yellow_y,
                                                          special_power_x,
                                                          special_power_y,
                                                          ghosts_can_be_eaten,
                                                          is_eaten_green,
                                                          is_eaten_red,
                                                          is_eaten_pink,
                                                          is_eaten_yellow,
                                                          is_eaten_player1,
                                                          is_eaten_player2, wait_signal, num_of_players), network_id)

        sleep(0.01)
        QGuiApplication.processEvents()


def send_data(network, network_id, player1_x, player1_y, player1_head_direction, lives_player1, green_x, green_y,
              red_x, red_y, pink_x, pink_y, yellow_x, yellow_y, special_power_x, special_power_y, ghosts_can_be_eaten,
              is_eaten_green, is_eaten_red, is_eaten_pink, is_eaten_yellow, is_eaten_player1, is_eaten_player2, wait_signal, num_of_players):
    data = None
    if network_id.value == 0:
        data = str(network_id.value) + "." + str(num_of_players) + ":" + str(player1_x.value) + "," + str(
            player1_y.value) + "," + str(player1_head_direction.value) + "," + \
               str(lives_player1.value) + "," + str(green_x.value) + "," + \
               str(green_y.value) + "," + str(red_x.value) + "," + \
               str(red_y.value) + "," + str(pink_x.value) + "," + \
               str(pink_y.value) + "," + str(yellow_x.value) + "," + \
               str(yellow_y.value) + "," + str(special_power_x.value) + "," + str(special_power_y.value) + \
               "," + str(ghosts_can_be_eaten.value) + "," + str(is_eaten_green.value) + \
               "," + str(is_eaten_red.value) + \
               "," + str(is_eaten_pink.value) + "," + str(is_eaten_yellow.value) + \
               "," + str(is_eaten_player1.value) + "," + str(is_eaten_player2.value)
    elif network_id.value == 1:
        data = str(network_id.value) + "." + str(num_of_players) + ":" + str(player1_x.value) + "," + str(
            player1_y.value) + "," + str(player1_head_direction.value) + "," + str(
            lives_player1.value)

    reply = network.send(data)

    if reply == "waiting for players":
        wait_signal.value = 1
       # wait_signal.value = 1
        if network_id.value == 0:
            return str(network_id.value) + ":" + str(-1) + "," + str(-1) + "," + str(1) + "," +  str(3)
        if network_id.value == 1:
            return str(network_id.value) + ":" + str(-1)+ "," +  str(-1) + "," + str(1) + "," + str(green_x.value) + \
                   + "," + str(green_y.value)+ "," + str(red_x.value)+ "," + str(red_y.value)+ "," + str(pink_x.value) + "," + str(pink_y.value) + \
                   + "," + str(yellow_x.value) + "," + str(yellow_y.value) +  "," + str(0)  + "," + str(0) +  "," + str(0)  + "," + str(0) +  "," + str(0)  + "," + str(0) +  "," + str(0)  + "," + str(0) +  "," + str(0)
    wait_signal.value = 0
    return reply


def parse_data(data, network_id):
    if network_id.value == 0:
        d = data.split(":")[1].split(",")
        return float(d[0]), float(d[1]), int(d[2]), int(d[3])
    elif network_id.value == 1:
        d = data.split(":")[1].split(",")
        return float(d[0]), float(d[1]), int(d[2]), int(d[3]), float(d[4]), float(d[5]), float(d[6]), float(d[7]), float(
            d[8]), float(d[9]), float(d[10]), float(d[11]), float(d[12]), float(d[13]), int(d[14]), int(d[15]),\
            int(d[16]), int(d[17]), int(d[18]), int(d[19]), int(d[20])
