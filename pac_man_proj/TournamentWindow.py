from PyQt5.QtWidgets import QMainWindow, QFrame, QDesktopWidget, QApplication, QPushButton, QWidget, QLabel, QSlider
from PyQt5.QtCore import Qt, QBasicTimer, pyqtSignal, QSize, QRect, QPropertyAnimation
from PyQt5.QtGui import QPainter, QColor, QImage, QPalette, QBrush, QIcon, QPixmap, QCursor
from PlayWindow import *
import sys
import pygame
from time import sleep
from settings import *


class TournamentWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.btn_four_player = QPushButton(self)
        self.btn_eight_player = QPushButton(self)
        self.waiting = QLabel(self)
        self.waiting_image = None

        self.init_ui()

    def init_ui(self):
        """initiates application UI"""
        self.setMinimumHeight(MAIN_WINDOW_HEIGHT)
        self.setMaximumHeight(MAIN_WINDOW_HEIGHT)
        self.setMinimumWidth(MAIN_WINDOW_WIDTH)
        self.setMaximumHeight(MAIN_WINDOW_WIDTH)
        self.resize(MAIN_WINDOW_HEIGHT, MAIN_WINDOW_WIDTH)
        self.center_window()
        self.setWindowTitle('Tournament')
        self.setStyleSheet("background-color: black;")
        self.waiting.setHidden(True)
        self.waiting_image = QPixmap("images\\waiting.png").scaled(200, 50)
        self.waiting.setPixmap(self.waiting_image)
        self.main_buttons()

    def center_window(self):
        """ postavlja prozor u centar bez obzira koja nam je velicina ekrana
         tj nije fiksno udaljen prozor recimo 500 piksela od vrha i sa strane leve"""
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2,
                  (screen.height() - size.height()) / 2)

    def main_buttons(self):
        """4 player button"""
        self.btn_four_player.resize(200, 50)
        self.btn_four_player.move(150, 185)
        """postavi sliku za btn"""
        four_player_btn_img = QIcon('images\\4_players.png')
        self.btn_four_player.setIcon(four_player_btn_img)
        self.btn_four_player.setIconSize(QSize(205, 50))
        """promeni izgled kursora kada se predje preko btn-a"""
        self.btn_four_player.setCursor(QCursor(Qt.PointingHandCursor))

        """8 players button"""

        self.btn_eight_player.resize(200, 50)
        self.btn_eight_player.move(150, 265)
        """postavi sliku za btn"""
        eight_player_btn_img = QIcon('images\\8_players.png')
        self.btn_eight_player.setIcon(eight_player_btn_img)
        self.btn_eight_player.setIconSize(QSize(205, 50))
        """promeni izgled kursora kada se predje preko btn-a"""
        self.btn_eight_player.setCursor(QCursor(Qt.PointingHandCursor))


        """funkcije za konektovanje btn-a i funkcije koja se odradjuje kada se klikne na njega"""
        self.btn_four_player.clicked.connect(self.four_player_button_clicked)
        self.btn_eight_player.clicked.connect(self.eight_player_button_clicked)

    def four_player_button_clicked(self):
        play_window = PlayWindow(3, 4)
        play_window.show()
        play_window.show_ready_image()
        sleep(1)
        QGuiApplication.processEvents()
        play_window.hide_ready_image()

        play_window.start_play()
        self.close()
    def eight_player_button_clicked(self):
        play_window = PlayWindow(3, 8)
        self.play_window.show()
        self.close()

