import pygame
from PyQt5.QtGui import QPixmap, QTransform, QPainter, QBrush, QPen
from PyQt5 import QtGui, QtCore
vec = pygame.math.Vector2


class Player(object):
    def __init__(self, app, label, start_position):
        self.app = app
        self.position = start_position
        self.start_position = start_position
        self.pixel_position = self.get_pixel_position()
        self.direction = vec(0, 0)
        self.last_direction = vec(0, 0)
        self.previous_direction = vec(0, 0)
        self.label = label
        self.image = QPixmap("images\\player_img.png").scaled(self.app.cell_height, self.app.cell_width)
        self.image2 = QPixmap("images\\playerMClosed_img.png").scaled(self.app.cell_height - 2, self.app.cell_width - 2)
        self.score = 0
        self.stop_player_movement = False
        # sluzi za ubrzavanje pacmana pri prelasku u drugi nivo #
        self.speed = 1
        self.lives = 3
        self.down = False
        self.up = False
        self.left = False
        self.right = False
        self.counter = 0
        self.slika = 1

    def get_pixel_position(self):
        return vec((self.position[0] * self.app.cell_width),
                   (self.position[1] * self.app.cell_height))

    # provera prethodnog pravca glave i na osnovu toga namesteni stepeni rotiranja
    def head_position(self, pravac):

        if pravac == 4:
            if not self.down:
                if self.right:
                    transform = QtGui.QTransform().rotate(90)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.down = True
                    self.right = False
                    self.up = False
                    self.left = False
                elif self.left:
                    # kada sam kliknula down pa right
                    transform = QtGui.QTransform().rotate(270)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.down = True
                    self.left = False
                    self.right = False
                    self.up = False
                elif self.up:
                    transform = QtGui.QTransform().rotate(180)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.down = True
                    self.up = False
                    self.right = False
                    self.left = False
                else:
                    transform = QtGui.QTransform().rotate(90)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.down = True
                self.image = pixmap

        elif pravac == 3:
            if not self.up:
                if self.right:
                    transform = QtGui.QTransform().rotate(270)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.up = True
                    self.right = False
                    self.down = False
                    self.left = False
                elif self.left:
                    transform = QtGui.QTransform().rotate(90)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.up = True
                    self.left = False
                    self.right = False
                    self.down = False
                elif self.down:
                    transform = QtGui.QTransform().rotate(180)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.up = True
                    self.down = False
                    self.right = False
                    self.left = False
                else:
                    transform = QtGui.QTransform().rotate(270)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.up = True
                self.image = pixmap

        elif pravac == 2:
            if not self.left:
                if self.right:
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(QTransform().scale(-1, 1))
                    self.left = True
                    self.right = False
                    self.down = False
                    self.up = False
                elif self.down:
                    transform = QtGui.QTransform().rotate(90)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.left = True
                    self.down = False
                    self.up = False
                    self.right = False
                elif self.up:
                    transform = QtGui.QTransform().rotate(270)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.left = True
                    self.up = False
                    self.right = False
                    self.down = False
                else:
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(QTransform().scale(-1, 1))
                    self.left = True
                self.image = pixmap

        elif pravac == 1:
            if not self.right:
                if self.left:
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(QTransform().scale(-1, 1))
                    self.right = True
                    self.left = False
                    self.down = False
                    self.up = False
                elif self.down:
                    transform = QtGui.QTransform().rotate(270)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.right = True
                    self.down = False
                    self.left = False
                    self.up = False
                elif self.up:
                    transform = QtGui.QTransform().rotate(90)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.right = True
                    self.up = False
                    self.down = False
                    self.left = False
                else:
                    transform = QtGui.QTransform().rotate(0)
                    pixmap = QtGui.QPixmap(self.image)
                    pixmap = pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)
                    self.right = True
                self.image = pixmap

        else:
            pass

    def draw(self, x_position, y_position, stopped_moving):

        self.pixel_position.x = x_position
        self.pixel_position.y = y_position

        if stopped_moving.value == 1:
            self.label.setPixmap(self.image)
        else:
            # counter se povecava prilikom poziva update
            # naizmenicno menjanje slike zatvorenih usta i otvorenih
            if self.counter % 20 == 0:
                self.slika += 1
            if self.slika % 2 == 0 or self.stop_player_movement:
                self.label.setPixmap(self.image)
            else:
                self.label.setPixmap(self.image2)

        self.label.setGeometry(x_position, y_position,
                               self.app.cell_width, self.app.cell_height)
        self.counter += 1

    def update_position(self):
        if self.pixel_position.x % self.app.cell_width == 0:
            self.position[0] = self.pixel_position.x // self.app.cell_width  # pomeri se za jednu celiju #
        if self.pixel_position.y % self.app.cell_height == 0:
            self.position[1] = self.pixel_position.y // self.app.cell_height

    def pick_up_coin(self, position, player_num):

        if vec(position) in self.app.coins:  # ukoliko moze da pojede novcic
            self.score += 10
            self.app.update_status_bar()
            self.app.coins.remove(position)

        if vec(position) in self.app.eat_ghosts_power_positions:  # ukoliko pojede moc kojom moze da jede duhove
            self.score += 100
            self.app.update_status_bar()
            self.app.eat_ghosts_power_positions.remove(position)
            self.app.green_ghost_mode.value = 2
            self.app.red_ghost_mode.value = 2
            self.app.can_eat_ghost.value = 500

        if vec(position) == self.app.special_power_position:  # ukoliko pojede visnju
            if self.app.number_of_players == 3:
                self.lives += 1
                if player_num == 1:
                    self.app.lives_player1.value += 1
                else:
                    self.app.lives_player2.value += 1

            else:
                self.app.lives += 1
            self.app.hide_special_power()
            self.app.update_status_bar()

        if self.app.high_score is None:
            self.app.high_score = self.score
            self.app.high_score_changed = True

        elif self.score > self.app.high_score:
            self.app.high_score = self.score
            self.app.high_score_changed = True



