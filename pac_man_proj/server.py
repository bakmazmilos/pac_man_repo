import socket
from _thread import *
import random
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server = '192.168.11.22'
port = 5555

server_ip = socket.gethostbyname(server)

try:
    s.bind((server, port))

except socket.error as e:
    print(str(e))

s.listen(8)
print("Waiting for a connection")

currentId = "0"
pos = ["0:20,20,1,220.0,260.0,320.0,300.0,220.0,300.0,320.0,260.0,-1,-1,0,0,0,0,0", "1:20,20,1",
       "2:20,20,1,220.0,260.0,320.0,300.0,220.0,300.0,320.0,260.0,-1,-1,0,0,0,0,0", "3:20,20,1",
       "4:20,20,1,220.0,260.0,320.0,300.0,220.0,300.0,320.0,260.0,-1,-1,0,0,0,0,0", "5:20,20,1",
       "6:20,20,1,220.0,260.0,320.0,300.0,220.0,300.0,320.0,260.0,-1,-1,0,0,0,0,0", "7:20,20,1"]

number_of_players = 0
matched_players = []
in_lobby = []
can_be_matched = []
first_entry = 0

def threaded_client(conn):
    global currentId, pos, number_of_players, first_entry

    conn.send(str.encode(currentId))
    current_id_int = int(currentId) + 1
    currentId = str(current_id_int)
    reply = 'waiting for players'
    while True:
        try:
            data = conn.recv(2048)
            reply = data.decode('utf-8')
            if not data:
                conn.send(str.encode("Goodbye"))
                break
            else:
                print("Recieved: " + reply)
                arr = reply.split(":")
                splited_arr = arr[0].split(".")
                id = int(splited_arr[0])
                torunament_number = int(splited_arr[1])
                pos[id] = reply

                if torunament_number == 2:
                    if id == 0: nid = 1
                    if id == 1: nid = 0
                if torunament_number == 4:
                    if first_entry == 0:
                        can_be_matched.append(1)
                        can_be_matched.append(2)
                        can_be_matched.append(3)
                        id_0_against = random.randint(1, 4)
                        matched_players.append([0, id_0_against])
                        can_be_matched.remove(id_0_against)
                        matched_players.append([can_be_matched[0], can_be_matched[1]])
                        first_entry = 1

                for x in matched_players:
                    if id == x[0]: nid = x[1]
                    if id == x[1]: nid = x[0]

                if torunament_number == 8:
                    pass

                if (number_of_players == 2 and torunament_number == 2) or (number_of_players == 4 and torunament_number == 4) \
                    or (number_of_players == 8 and torunament_number == 8):
                    reply = pos[nid][:]
                else:
                    reply = "waiting for players"

            print("Sending: " + reply)
            conn.sendall(str.encode(reply))

        except:
            break

    print("Connection Closed")
    conn.close()


while True:
    conn, addr = s.accept()
    print("Connected to: ", addr)

    number_of_players += 1

    start_new_thread(threaded_client, (conn,))
