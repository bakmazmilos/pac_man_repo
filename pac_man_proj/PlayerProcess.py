from Player import vec
from time import sleep
from PyQt5.QtGui import QGuiApplication

player_previous_direction = vec(0, 0)
player_last_direction = vec(0, 0)
player_direction = vec(0, 0)
player_position = [0, 0]
app_walls = []
player_x_position = None
player_y_position = None
player_stopped_moving = 0
x_start_position = None
y_start_position = None


def move_player(x_position, y_position, position, walls, key_queue, exit_queue, head_direction, stopped_moving, eaten_player):
    run = True

    global player_direction
    player_direction = vec(0, 0)

    global player_x_position
    player_x_position = x_position.value

    global player_y_position
    player_y_position = y_position.value

    global app_walls
    app_walls = walls

    global player_position
    player_position = position

    global x_start_position
    x_start_position = x_position.value

    global y_start_position
    y_start_position = y_position.value

    while run:
        sleep(0.01)
        QGuiApplication.processEvents()

        if not exit_queue.empty():
            return

        if key_queue.value == 1:
            player_direction = vec(0, 1)
        if key_queue.value == 2:
            player_direction = vec(1, 0)
        if key_queue.value == 3:
            player_direction = vec(0, -1)
        if key_queue.value == 4:
            player_direction = vec(-1, 0)

        if eaten_player.value == 1:
            start_again()
            head_direction.value = get_head_direction()
            x_position.value = player_x_position
            y_position.value = player_y_position
            global player_stopped_moving
            player_stopped_moving = 1
            stopped_moving.value = player_stopped_moving
            key_queue.value = -1
            sleep(3)
            QGuiApplication.processEvents()

        move()

        stopped_moving.value = player_stopped_moving

        if player_direction == [0, 0]:
            stopped_moving.value = 1

        head_direction.value = get_head_direction()
        x_position.value = player_x_position
        y_position.value = player_y_position


def get_head_direction():
    global player_direction

    if player_direction == [1, 0]:
        return 1    # desno
    if player_direction == [-1, 0]:
        return 2    # levo
    if player_direction == [0, -1]:
        return 3    # gore
    if player_direction == [0, 1]:
        return 4    # dole
    if player_direction == [0, 0]:  # kad se resetuju stavi ga da gleda desno
        return 1


def update():
    # is_tunel proverava da li se nalazi na izlazu iz tunela, i da li treba da se pojavi na drugoj strani #
    if is_in_tunnel():
        # postavljanje koordinata pacmana na osnovu celija #
        global player_x_position
        player_x_position = player_position[0] * 20
        global player_y_position
        player_y_position = player_position[1] * 20

    player_x_position += player_direction[0]
    player_y_position += player_direction[1]

    # ukoliko je pri deljenju bez ostatka, to znaci da smo presli na drugu celiju #
    if player_x_position % 20 == 0:
        player_position[0] = player_x_position // 20  # pomeri se za jednu celiju #
    if player_y_position % 20 == 0:
        player_position[1] = player_y_position // 20


def move():
    global player_stopped_moving
    if can_move():
        update()
        global player_last_direction
        player_last_direction = player_direction
        global player_previous_direction
        player_previous_direction = player_last_direction
        player_stopped_moving = 0
    else:
        player_stopped_moving = 1


def can_move():
    # proverava se ono sto i naziv funkcije kaze, ukoliko je 1 na sledecoj zadatoj poziciji, ne moze da se pomeri #
    global player_direction
    global player_last_direction
    for wall in app_walls:
        if vec(player_position + player_direction) == wall:
            if can_continue_moving():
                # na ovaj nacin ce da nastavi u smeru u kom se kretao #
                player_direction = player_previous_direction
                player_last_direction = player_previous_direction
                return True
            return False
        # to je onaj prolaz za duhove odakle oni izlaze
    if vec(player_position + player_direction) == vec(13, 12) or \
       vec(player_position + player_direction) == vec(14, 12):
        player_direction = player_previous_direction
        player_last_direction = player_previous_direction
        return True
    global player_x_position
    global player_y_position
    if player_x_position % 20 != 0 or player_y_position % 20 != 0:  # ovde ce da dodje ukoliko "moze" da skrene
        # recimo ukoliko je pozicija na y osi 535 pacman je na istoj POSITION kao i u 520, ali ne treba tad da skrene
        # vec tek kad bude na 520.. ako nije jasno objasnicu usmeno(milos)
        # za pozicije koje su obe i x i y %20 == 0 ne proveravam jer tada MOZE da skrene tipa 20,20
        if not (player_direction.x + 2 == player_previous_direction.x or
                player_direction.x - 2 == player_previous_direction.x or
                player_direction.y + 2 == player_previous_direction.y or
                player_direction.y - 2 == player_previous_direction.y):
            player_direction = player_previous_direction
            player_last_direction = player_previous_direction
    return True


def can_continue_moving():
    for wall in app_walls:
        if vec(player_position + player_previous_direction) == wall:
            return False
    return True


def is_in_tunnel():
    # na odredjenim koordinatama u tunelu pacman treba da se pojavi na pocetku drugog tunela #
    # ovo je za levi tunel #
    global player_position
    if player_position == [28, 14] and player_direction == vec(1, 0):
        player_position = [-1, 14]
        return True
    # ovo je za desni tunel #
    if player_position == [-1, 14] and player_direction == vec(-1, 0):
        player_position = [28, 14]  # pojavi se na levom tunelu #
        return True
    return False


def start_again():
    # postavljeni svi na startne pozicije
    global x_start_position
    global y_start_position
    global player_x_position
    player_x_position = x_start_position
    global player_y_position
    player_y_position = y_start_position
    global player_direction
    player_direction = vec(0, 0)
    global player_position
    player_position[0] = int(x_start_position // 20)
    player_position[1] = int(y_start_position // 20)


