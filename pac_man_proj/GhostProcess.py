from Player import vec
from time import sleep
from PyQt5.QtGui import QGuiApplication
import random
from threading import Thread

ghost_previous_direction = vec(0, 0)
ghost_last_direction = vec(0, 0)
ghost_direction = vec(0, 0)
ghost_position = [0, 0]
ghost_start_position = [0, 0]
app_walls = []
ghost_x_position = None
ghost_y_position = None
ghost_stopped_moving = 0
ghost_next_direction = vec(0, 0)
target_player1_position = vec(0, 0)
target_player2_position = vec(0, 0)
player1_pixel_position = [0, 0]
player2_pixel_position = [0, 0]
mode = 0  # 0 - normal mode, 1 - chase mode, 2 - scared mode
path_to_target = None
is_eaten_player1 = 0
is_eaten_player2 = 0
is_eaten_ghost = 0
can_be_eaten = 0
ghost_start_position_x = None
ghost_start_position_y = None
speed = 0


def do_ghost_mode(ghost_x, ghost_y, ghost_cell_position, ghost_mode, player_position_x, player_position_y, walls,
                  eaten_player1, eaten_player2, can_eat_ghost, eaten_ghost, player2_position_x, player2_position_y,
                  ghost_speed):
    global mode
    mode = ghost_mode.value

    global app_walls
    app_walls = walls

    global player1_pixel_position
    player1_pixel_position = [player_position_x.value, player_position_y.value]

    global player2_pixel_position
    player2_pixel_position = [player2_position_x.value, player2_position_y.value]

    global ghost_position
    ghost_position = ghost_cell_position

    global ghost_x_position
    ghost_x_position = ghost_x.value

    global ghost_y_position
    ghost_y_position = ghost_y.value

    global is_eaten_player1
    is_eaten_player1 = eaten_player1.value

    global is_eaten_player2
    is_eaten_player2 = eaten_player2.value

    global is_eaten_ghost
    is_eaten_ghost = eaten_ghost.value

    global can_be_eaten
    can_be_eaten = can_eat_ghost.value

    global ghost_start_position_x
    ghost_start_position_x = ghost_x.value

    global ghost_start_position_y
    ghost_start_position_y = ghost_y.value

    global  speed
    speed = ghost_speed.value

    global ghost_start_position
    ghost_start_position = [int(ghost_start_position_x // 20), int(ghost_start_position_y // 20)]

    thread = Thread(target=get_bfs_path, args=(1,))
    thread.daemon = True
    thread.start()

    global target_player1_position

    run = True

    while run:
        speed = ghost_speed.value
        if eaten_ghost.value == 1:
            sleep(0.005)
        elif can_be_eaten > 0 and eaten_ghost.value == 0:
            sleep(0.028)
        elif can_be_eaten == 0:
            sleep(0.02)
        QGuiApplication.processEvents()
        target_player1_position = vec(int(player_position_x.value // 20), int(player_position_y.value // 20))

        player1_pixel_position = [player_position_x.value, player_position_y.value]
        player2_pixel_position = [player2_position_x.value, player2_position_y.value]

        """if eaten_player1.value == 1 or eaten_player2.value == 1:
            start_again()
            sleep(3)
            QGuiApplication.processEvents()"""

        mode = ghost_mode.value

        if can_be_eaten == 0:
            if touched_pacman():
                eaten_player1.value = is_eaten_player1
                eaten_player2.value = is_eaten_player2
                start_again()
                sleep(3)
                QGuiApplication.processEvents()
                eaten_player1.value = 0
                is_eaten_player1 = 0
                eaten_player2.value = 0
                is_eaten_player2 = 0
        else:
            if touched_pacman():
                eaten_ghost.value = 1
                ghost_mode.value = 0
                mode = 0

        if mode == 1:
            target_player1_position = get_target()
            chase_mode()
        elif mode == 0:
            if eaten_ghost.value == 0:
                normal_mode()
            else:
                target_player1_position = get_target()
                if chase_mode():
                    sleep(2)
                    eaten_ghost.value = 0
                    number = random.random()
                    if number < 0.66:
                        ghost_mode.value = 0
                    else:
                        ghost_mode.value = 1

        elif mode == 2:
            if can_eat_ghost.value == 0:
                ghost_mode.value = 1
            else:
                target_player1_position = get_target()
                scared_mode()

        ghost_x.value = ghost_x_position
        ghost_y.value = ghost_y_position
        can_be_eaten = can_eat_ghost.value
        is_eaten_ghost = eaten_ghost.value


def chase_mode():
    global ghost_last_direction
    global ghost_next_direction
    global ghost_direction
    ghost_next_direction = get_path_direction()
    if ghost_next_direction == vec(-2, -2):
        return
    if ghost_next_direction == vec(-3, -3):
        return True
    ghost_last_direction = ghost_next_direction
    ghost_direction = ghost_next_direction
    move()


def normal_mode():
    get_random_direction()
    move()


def get_random_direction():
    global ghost_last_direction

    if can_change_direction():
        number = random.random()
        if number < 0.25:
            ghost_last_direction = vec(0, 1)
        elif 0.25 <= number < 0.5:
            ghost_last_direction = vec(0, -1)
        elif 0.5 <= number < 0.75:
            ghost_last_direction = vec(-1, 0)
        else:
            ghost_last_direction = vec(1, 0)
            # ovde treba odraditi vracanje pacmana i duhova na prvobitna mesta a to je sacuvano u start_position
            # skinuti jedan zivot pacmanu
        global ghost_direction
        ghost_direction = ghost_last_direction


def scared_mode():
    global ghost_last_direction
    global ghost_next_direction
    global ghost_direction
    ghost_next_direction = get_path_direction()
    if ghost_next_direction == vec(-2, -2):
        return
    ghost_last_direction = ghost_next_direction
    ghost_direction = ghost_next_direction
    move()


# dobijanje smera u kom treba da se krece pacman (1, 0) npr za desno kretanje
def get_path_direction():
    next_cell = find_next_cell_in_path()
    if next_cell == [-2, -2]:
        return vec(-2, -2)
    if next_cell == [-3, -3]:
        return vec(-3, -3)
    x_direction = next_cell[0] - ghost_position[0]
    y_direction = next_cell[1] - ghost_position[1]
    return vec(x_direction, y_direction)


def find_next_cell_in_path():
    global path_to_target
    global mode
    if path_to_target is None:
        return [-2, -2]
    if len(path_to_target) <= 1:
        if mode == 2:
            return path_to_target[0]
        return [-3, -3]
    return path_to_target[1]


def get_bfs_path(n):
    run = True

    while run:
        sleep(0.02)
        QGuiApplication.processEvents()
        global path_to_target
        if target_player1_position[0] == -1:
            target_player1_position[0] = 1
        if target_player1_position[0] == 28:
            target_player1_position[0] = 1
        path_to_target = BFS([int(ghost_position[0]), int(ghost_position[1])], [
            int(target_player1_position[0]), int(target_player1_position[1])])


def BFS(start, target):
    grid = [[0 for x in range(28)] for x in range(30)]
    for cell in app_walls:
        if cell.x < 28 and cell.y < 30:
            grid[int(cell.y)][int(cell.x)] = 1
    queue = [start]
    path = []
    visited = []
    while queue:
        current = queue[0]
        queue.remove(queue[0])
        visited.append(current)
        if current == target:
            break
        else:
            neighbours = [[0, -1], [1, 0], [0, 1], [-1, 0]]
            for neighbour in neighbours:
                if neighbour[0] + current[0] >= 0 and neighbour[0] + current[0] < len(grid[0]):
                    if neighbour[1] + current[1] >= 0 and neighbour[1] + current[1] < len(grid):
                        next_cell = [neighbour[0] + current[0], neighbour[1] + current[1]]
                        if next_cell not in visited:
                            if grid[next_cell[1]][next_cell[0]] != 1:
                                queue.append(next_cell)
                                path.append({"Current": current, "Next": next_cell})
    shortest = [target]
    while target != start:
        for step in path:
            if step["Next"] == target:
                target = step["Current"]
                shortest.insert(0, step["Current"])
    return shortest


def can_change_direction():
    global ghost_x_position
    global ghost_y_position

    if ghost_x_position % 20 != 0:
        return False
    if ghost_y_position % 20 != 0:
        return False
    return True


def get_target():
    global target_player1_position
    global ghost_start_position
    if mode == 1:
        return target_player1_position
    elif mode == 2:
        # 28 broj kolona. 30 broj redova
        if target_player1_position[0] > 28 // 2 and target_player1_position[1] > 30 // 2:
            return vec(1, 1)
        if target_player1_position[0] > 28 // 2 and target_player1_position[1] < 30 // 2:
            return vec(1, 30 - 2)
        if target_player1_position[0] < 28 // 2 and target_player1_position[1] > 30 // 2:
            return vec(28 - 2, 1)
        else:
            return vec(28 - 2, 30 - 2)
    elif mode == 0:
        return ghost_start_position


def update():
    # is_tunel proverava da li se nalazi na izlazu iz tunela, i da li treba da se pojavi na drugoj strani #
    global ghost_x_position
    global ghost_y_position
    global speed

    if is_in_tunnel():
        # postavljanje koordinata pacmana na osnovu celija #
        ghost_x_position = ghost_position[0] * 20
        ghost_y_position = ghost_position[1] * 20

    ghost_x_position += ghost_direction[0] * speed
    ghost_y_position += ghost_direction[1] * speed

    # ukoliko je pri deljenju bez ostatka, to znaci da smo presli na drugu celiju #
    if ghost_x_position % 20 == 0:
        ghost_position[0] = ghost_x_position // 20  # pomeri se za jednu celiju #
    if ghost_y_position % 20 == 0:
        ghost_position[1] = ghost_y_position // 20


def move():
    if can_move():
        update()
        global ghost_last_direction
        ghost_last_direction = ghost_direction
        global ghost_previous_direction
        ghost_previous_direction = ghost_last_direction


def can_move():
    # proverava se ono sto i naziv funkcije kaze, ukoliko je 1 na sledecoj zadatoj poziciji, ne moze da se pomeri #
    global ghost_direction
    global ghost_last_direction
    for wall in app_walls:
        if vec(ghost_position + ghost_direction) == wall:
            if can_continue_moving():
                # na ovaj nacin ce da nastavi u smeru u kom se kretao #
                ghost_direction = ghost_previous_direction
                ghost_last_direction = ghost_previous_direction
                return True
            return False
    global ghost_x_position
    global ghost_y_position

    if ghost_x_position % 20 != 0 or ghost_y_position % 20 != 0:  # ovde ce da dodje ukoliko "moze" da skrene
        # recimo ukoliko je pozicija na y osi 535 pacman je na istoj POSITION kao i u 520, ali ne treba tad da skrene
        # vec tek kad bude na 520.. ako nije jasno objasnicu usmeno(milos)
        # za pozicije koje su obe i x i y %20 == 0 ne proveravam jer tada MOZE da skrene tipa 20,20
        if not (ghost_direction.x + 2 == ghost_previous_direction.x or
                ghost_direction.x - 2 == ghost_previous_direction.x or
                ghost_direction.y + 2 == ghost_previous_direction.y or
                ghost_direction.y - 2 == ghost_previous_direction.y):  # ovde nastavlja u prethodnom pravcu ukoliko nije mogao
            # da skrene SEM ako odluci da se okrene, tipa sa -1 na x da predje na 1 na x, to dozvoljavamghost_direction = ghost_previous_direction
            ghost_direction = ghost_previous_direction
            ghost_last_direction = ghost_previous_direction
    return True


# ukoliko nije moguce skretanje u jednu od strana, ova fja proverava da li moze da nastavi u istom smeru #
# u kom se kretao #
def can_continue_moving():
    for wall in app_walls:
        if vec(ghost_position + ghost_previous_direction) == wall:
            return False
    return True


def is_in_tunnel():
    # na odredjenim koordinatama u tunelu pacman treba da se pojavi na pocetku drugog tunela #
    # ovo je za levi tunel #
    global ghost_position
    if ghost_position == [28, 14] and ghost_direction == vec(1, 0):
        ghost_position = [-1, 14]
        return True
    # ovo je za desni tunel #
    if ghost_position == [-1, 14] and ghost_direction == vec(-1, 0):
        ghost_position = [28, 14]  # pojavi se na levom tunelu #
        return True
    return False


def touched_pacman():
    global ghost_x_position
    global ghost_y_position
    global player1_pixel_position
    global player2_pixel_position
    global is_eaten_player1
    global is_eaten_player2
    global can_be_eaten
    if ((ghost_x_position + 15 >= player1_pixel_position[0] >= ghost_x_position + 5) or
       (ghost_x_position - 15 <= player1_pixel_position[0] <= ghost_x_position - 5))and \
       ghost_y_position == player1_pixel_position[1]:
        if can_be_eaten == 1:
            return True
        is_eaten_player1 = 1
        return True
    if ((ghost_y_position + 15 >= player1_pixel_position[1] >= ghost_y_position + 5) or
       (ghost_y_position - 15 <= player1_pixel_position[1] <= ghost_y_position - 5))and \
       ghost_x_position == player1_pixel_position[0]:
        if can_be_eaten == 1:
            return True
        is_eaten_player1 = 1
        return True

    # provera za drugog pacmana
    if [player2_pixel_position[0], player2_pixel_position[1]] != [0.0, 0.0]:
        if ((ghost_x_position + 15 >= player2_pixel_position[0] >= ghost_x_position + 5) or
           (ghost_x_position - 15 <= player2_pixel_position[0] <= ghost_x_position - 5)) and \
           ghost_y_position == player2_pixel_position[1]:
            if can_be_eaten == 1:
                return True
            is_eaten_player2 = 1
            return True
        if ((ghost_y_position + 15 >= player2_pixel_position[1] >= ghost_y_position + 5) or
           (ghost_y_position - 15 <= player2_pixel_position[1] <= ghost_y_position - 5)) and \
           ghost_x_position == player2_pixel_position[0]:
            if can_be_eaten == 1:
                return True
            is_eaten_player2 = 1
            return True

    return False


def start_again():
    global player2_pixel_position
    global ghost_x_position
    ghost_x_position = ghost_start_position_x
    global ghost_y_position
    ghost_y_position = ghost_start_position_y
    global ghost_position
    ghost_position = [int(ghost_start_position_x // 20), int(ghost_start_position_y // 20)]
    global is_eaten_player1
    global is_eaten_player2
    if is_eaten_player1 == 1:
        is_eaten_player1 = 0
    if [player2_pixel_position[0], player2_pixel_position[1]] != [0.0, 0.0]:
        if is_eaten_player2 == 2:
            is_eaten_player2 = 0
