from PyQt5.QtWidgets import QMainWindow, QFrame, QDesktopWidget, QApplication, QPushButton, QWidget, QLabel, QSlider
from PyQt5.QtCore import Qt, QBasicTimer, pyqtSignal, QSize, QRect, QPropertyAnimation
from PyQt5.QtGui import QPainter, QColor, QImage, QPalette, QBrush, QIcon, QPixmap, QCursor
from PlayWindow import *
from TournamentWindow import *
import sys
import pygame
from time import sleep


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.logo_label = QLabel(self)
        self.btn_one_player = QPushButton(self)
        self.btn_two_player = QPushButton(self)
        self.btn_multi_player = QPushButton(self)
        self.btn_tournament = QPushButton(self)

        self.play_window = None
        self.tournament_window = None
        pygame.init()
        self.init_ui()

    def init_ui(self):
        """initiates application UI"""
        self.setMinimumHeight(MAIN_WINDOW_HEIGHT)
        self.setMaximumHeight(MAIN_WINDOW_HEIGHT)
        self.setMinimumWidth(MAIN_WINDOW_WIDTH)
        self.setMaximumHeight(MAIN_WINDOW_WIDTH)
        self.resize(MAIN_WINDOW_HEIGHT, MAIN_WINDOW_WIDTH)
        self.center_window()
        self.setWindowTitle('PAC-MAN')
        self.set_background()
        self.main_buttons()
        pygame.mixer.music.load("pacman_beginning.wav")
        pygame.mixer.music.set_volume(0.0)
        pygame.mixer.music.play(-1)
        self.set_volume()

    def main_buttons(self):
        """one player button"""
        self.btn_one_player.resize(200, 50)
        self.btn_one_player.move(150, 200)
        """postavi sliku za btn"""
        one_player_btn_img = QIcon('images\\1Player_img.png')
        self.btn_one_player.setIcon(one_player_btn_img)
        self.btn_one_player.setIconSize(QSize(205, 50))
        """promeni izgled kursora kada se predje preko btn-a"""
        self.btn_one_player.setCursor(QCursor(Qt.PointingHandCursor))

        """two players button"""

        self.btn_two_player = QPushButton(self)
        self.btn_two_player.resize(200, 50)
        self.btn_two_player.move(150, 250)
        self.btn_two_player.setCursor(QCursor(Qt.PointingHandCursor))
        """postavi sliku za btn"""
        two_player_btn_img = QIcon('images\\2Player_img.png')
        self.btn_two_player.setIcon(two_player_btn_img)
        self.btn_two_player.setIconSize(QSize(205, 50))

        # multiplayer button

        self.btn_multi_player.resize(200, 50)
        self.btn_multi_player.move(150, 300)
        self.btn_multi_player.setCursor(QCursor(Qt.PointingHandCursor))
        """postavi sliku za btn"""
        multi_player_btn_img = QIcon('images\\multiplayer_img.png')
        self.btn_multi_player.setIcon(multi_player_btn_img)
        self.btn_multi_player.setIconSize(QSize(205, 50))

        # tournament button

        self.btn_tournament.resize(200, 50)
        self.btn_tournament.move(150, 350)
        self.btn_tournament.setCursor(QCursor(Qt.PointingHandCursor))
        """postavi sliku za btn"""
        torunament_btn_img = QIcon('images\\tournament_img.png')
        self.btn_tournament.setIcon(torunament_btn_img)
        self.btn_tournament.setIconSize(QSize(205, 50))

        """funkcije za konektovanje btn-a i funkcije koja se odradjuje kada se klikne na njega"""
        self.btn_one_player.clicked.connect(self.one_player_button_clicked)
        self.btn_two_player.clicked.connect(self.two_player_button_clicked)
        self.btn_multi_player.clicked.connect(self.multi_player_button_clicked)
        self.btn_tournament.clicked.connect(self.tournament_button_clicked)


    """funkcija kada se klikne one player btn"""
    def one_player_button_clicked(self):
        pygame.mixer.music.stop()
        self.play_window = PlayWindow(1, 0)
        self.play_window.show()

        self.play_window.show_ready_image()
        sleep(1)
        QGuiApplication.processEvents()
        self.play_window.hide_ready_image()

        self.play_window.start_play()
        self.hide()

    """funkcija kada se klikne two player btn"""
    def two_player_button_clicked(self):
        pygame.mixer.music.stop()
        """parametar playwindow klase je broj igraca za igru( 1 ili 2)"""
        self.play_window = PlayWindow(2, 0)
        self.play_window.show()

        self.play_window.show_ready_image()
        sleep(1)
        QGuiApplication.processEvents()
        self.play_window.hide_ready_image()

        self.play_window.start_play()
        self.hide()

    def multi_player_button_clicked(self):
        pygame.mixer.music.stop()
        self.play_window = PlayWindow(3, 2)
        self.play_window.show()

        self.play_window.show_ready_image()
        sleep(1)
        QGuiApplication.processEvents()
        self.play_window.hide_ready_image()

        self.play_window.start_play()
        self.hide()

    def tournament_button_clicked(self):
        pygame.mixer.music.stop()
        self.tournament_window = TournamentWindow()
        self.tournament_window.show()
        self.hide()

    def set_background(self):
        self.setStyleSheet("background-color: black;")
        self.logo_label.setPixmap(QPixmap("images\\pacman_logo.jpg"))
        self.logo_label.setGeometry(50, 50, 400, 100)

    def center_window(self):
        """ postavlja prozor u centar bez obzira koja nam je velicina ekrana
         tj nije fiksno udaljen prozor recimo 500 piksela od vrha i sa strane leve"""
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2,
                  (screen.height() - size.height()) / 2)

    def set_volume(self):
        self.mute = QPixmap('images\\sound-mute-30x.png')
        self.min = QPixmap('images\\sound-min-30x.png')
        self.med = QPixmap('images\\sound-med-30x.png')
        self.max = QPixmap('images\\sound-max-30x.png')

        """dodat slider za zvuk"""
        sld = QSlider(Qt.Horizontal, self)
        sld.setFocusPolicy(Qt.NoFocus)
        sld.setGeometry(190, 440, 70, 30)
        sld.valueChanged.connect(self.change_value)
        """labela u kojoj se nalazi slika zvucnika"""
        self.label = QLabel(self)
        self.label.setPixmap(self.mute)
        self.label.setGeometry(280, 440, 30, 30)
        self.label.setStyleSheet("background-color: rgb(60,171,255)")

        """u zavisnosti od pozicije na slideru se smenjuju slike u labeli zaduzenoj za sliku zvucnika"""
    def change_value(self, value):

        if value == 0:
            self.label.setPixmap(self.mute)
            pygame.mixer.music.set_volume(0.0)
        elif 0 < value <= 10:
            pygame.mixer.music.set_volume(0.1)
        elif 0 < value <= 20:
            pygame.mixer.music.set_volume(0.2)
        elif 0 < value <= 30:
            self.label.setPixmap(self.min)
            pygame.mixer.music.set_volume(0.3)
        elif 0 < value <= 40:
            pygame.mixer.music.set_volume(0.4)
        elif 0 < value <= 50:
            pygame.mixer.music.set_volume(0.5)
        elif 0 < value <= 60:
            pygame.mixer.music.set_volume(0.6)
        elif 0 < value <= 70:
            pygame.mixer.music.set_volume(0.7)
        elif 30 < value < 80:
            self.label.setPixmap(self.med)
            pygame.mixer.music.set_volume(0.8)
        elif 0 < value <= 90:
            pygame.mixer.music.set_volume(0.9)
        else:
            self.label.setPixmap(self.max)
            pygame.mixer.music.set_volume(1.0)
