from MainWindow import *
from PyQt5.QtCore import QSize, QFile, QTimer
from PyQt5.QtWidgets import QMainWindow, QLabel
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPainter, QFont, QPen, QTransform, QGuiApplication, QMovie
from multiprocessing import Process, Value, Queue
from settings import *
from Player import *
from Ghost import *
from PlayerProcess import *
from GhostProcess import *
import random
from MultiPlayerProcess import *
import time

Vector = pygame.math.Vector2


class PlayWindow(QMainWindow):
    def __init__(self, num_of_players, tournament_players_number):  # ovde treba proslediti broj igraza u turniru dal 4 ili 8
        super().__init__()

        self.tournament_num_of_players = tournament_players_number # i ovde dodati

        self.cell_width = PLAYING_WINDOW_WIDTH // 28
        self.cell_height = (PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT) // 30
        self.background_image = QPixmap("images\\playing_background.png")

        self.number_of_players = num_of_players

        self.status_bar = QLabel(self)
        self.status_bar_player2 = QLabel(self)
        self.high_score_bar = QLabel(self)

        self.player_ready = QLabel(self)
        self.player_ready.setHidden(True)
        self.ready_image = QPixmap("images\\ready.jpg").scaled(180, 30)
        self.player_ready.setPixmap(self.ready_image)

        self.game_over_label = QLabel(self)
        self.game_over_label.setHidden(True)
        """36 LINIJA (VISINA, SIRINA)"""
        self.game_over_image = QPixmap("images\\pac-man-game-over.png").scaled(180, 30)
        self.game_over_label.setPixmap(self.game_over_image)

        self.high_score = self.get_highest_score()
        self.high_score_changed = False

        self.player1 = None
        self.player2 = None

        self.last_key = Qt.Key_R
        self.label_for_coins = []
        self.player1_last_score = 0
        self.player2_last_score = 0

        self.ready = False

        self.lives = 3
        self.lives_up_to_date = True
        #
        self.eat_ghosts_power_label = QLabel(self)
        self.eat_ghosts_power_label.setHidden(True)
        self.power_img = QPixmap("images\\cherry.jpg").scaled(20, 20)
        self.eat_ghosts_power_label.setPixmap(self.power_img)
        self.eat_ghosts_power_positions = []

        # prvi parametar je koliko u sekundama da se prikazije, drugo nakon koliko da se pojavi opet
        self.special_power_thread = Thread(target=self.generate_random_power, args=(10, 15))

        self.wait_signal = Value('i')
        self.wait_signal.value = 1
        self.path_cells = []
        self.was_a_coin = False

        self.walls = []
        self.ghost_passage = []
        self.coins = []
        self.start_coins = []
        self.ghosts_position = []

        self.player_start_position = None
        self.player2_start_position = None

        self.down = False
        self.up = False
        self.left = False
        self.right = False

        self.green_ghost = None
        self.pink_ghost = None
        self.yellow_ghost = None
        self.red_ghost = None

        self.green_ghost_label = QLabel(self)
        self.red_ghost_label = QLabel(self)
        self.pink_ghost_label = QLabel(self)
        self.yellow_ghost_label = QLabel(self)

        self.green_ghost_process = None
        self.red_ghost_process = None
        self.pink_ghost_process = None
        self.yellow_ghost_process = None

        self.player2_label = QLabel(self)
        self.player1_label = QLabel(self)

        self.player1_process = None
        self.player2_process = None

        self.read_walls()  # ucitavanje jedinica iz fajla u promenljivu walls

        self.key_queue = Value('i')
        self.key_queue_player2 = Value('i')
        self.exit_queue = Queue()

        self.head_direction_player1 = Value('i')  # vrednost 1: dole, 2: desno, 3: gore, 4: levo
        self.x_position_player1 = Value('f')
        self.y_position_player1 = Value('f')
        self.is_eaten_player1 = Value('i')
        self.lives_player1 = Value('i')
        self.lives_player1.value = 3

        # PLAYER 2 ---------------------------------------
        self.head_direction_player2 = Value('i')
        self.x_position_player2 = Value('f')
        self.y_position_player2 = Value('f')
        self.is_eaten_player2 = Value('i')
        self.lives_player2 = Value('i')
        self.lives_player2.value = 3

        self.stopped_moving_player1 = Value('i')
        self.stopped_moving_player1.value = 1
        self.stopped_moving_player2 = Value('i')
        self.stopped_moving_player2.value = 1

        # red ghost
        self.x_position_red_ghost = Value('f')
        self.y_position_red_ghost = Value('f')
        self.red_ghost_mode = Value('i')
        self.is_eaten_red_ghost = Value('i')

        # yelllow ghost
        self.x_position_yellow_ghost = Value('f')
        self.y_position_yellow_ghost = Value('f')
        self.yellow_ghost_mode = Value('i')
        self.is_eaten_yellow_ghost = Value('i')

        # pink ghost
        self.x_position_pink_ghost = Value('f')
        self.y_position_pink_ghost = Value('f')
        self.pink_ghost_mode = Value('i')
        self.is_eaten_pink_ghost = Value('i')

        # green ghost
        self.x_position_green_ghost = Value('f')
        self.y_position_green_ghost = Value('f')
        self.green_ghost_mode = Value('i')
        self.is_eaten_green_ghost = Value('i')

        self.can_eat_ghost = Value('i')

        self.process_multiplayer = None
        self.network_id = Value('i')

        self.special_power_pixel_position_x = Value('f')
        self.special_power_pixel_position_y = Value('f')
        self.special_power_pixel_position_x.value = -1
        self.special_power_pixel_position_y.value = -1
        self.special_power_position = Vector(-1, -1)

        self.ghost_speed = Value('f')
        self.ghost_speed.value = 1
        self.brzina = False

        self.game_over_space_key = 0
        self.game_is_over = False

        self.setup_ui()

    def paintEvent(self, event):

        painter = QPainter(self)
        painter.setBrush(QBrush(Qt.yellow, Qt.SolidPattern))
        for coin in self.coins:
            if coin not in self.eat_ghosts_power_positions:
                painter.drawEllipse(coin.x * self.cell_width + 7, coin.y * self.cell_height + 7, 7, 7)

        # moc kojom moze da pojede duha
        painter.setBrush(QBrush(Qt.darkYellow, Qt.SolidPattern))
        for power in self.eat_ghosts_power_positions:
            painter.drawEllipse(power[0] * self.cell_width + 5, power[1] * self.cell_height + 5, 12, 12)

    def setup_ui(self):
        self.setFixedSize(PLAYING_WINDOW_WIDTH, PLAYING_WINDOW_HEIGHT)

        self.set_players()
        self.set_status_bars()
        self.set_ghosts()
        self.center_window()

        image = self.background_image.scaled(QSize(PLAYING_WINDOW_WIDTH, (PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT)))
        palette = QPalette()
        palette.setBrush(QPalette.Background, QBrush(image))

        self.setPalette(palette)

    def start_play(self):  # ovu fju pozivamo iz MainWindw klase nakon sto se prikaze prozor PlayWindow

        if self.number_of_players == 3:
            self.process_multiplayer = Process(target=do_multiplayer, args=(self.network_id, self.x_position_player1,
                                                                            self.y_position_player1,
                                                                            self.head_direction_player1,
                                                                            self.lives_player1,
                                                                            self.x_position_player2,
                                                                            self.y_position_player2,
                                                                            self.head_direction_player2,
                                                                            self.lives_player2,
                                                                            self.x_position_green_ghost,
                                                                            self.y_position_green_ghost,
                                                                            self.x_position_red_ghost,
                                                                            self.y_position_red_ghost,
                                                                            self.x_position_pink_ghost,
                                                                            self.y_position_pink_ghost,
                                                                            self.x_position_yellow_ghost,
                                                                            self.y_position_yellow_ghost,
                                                                            self.special_power_pixel_position_x,
                                                                            self.special_power_pixel_position_y,
                                                                            self.can_eat_ghost,
                                                                            self.is_eaten_green_ghost,
                                                                            self.is_eaten_red_ghost,
                                                                            self.is_eaten_yellow_ghost,
                                                                            self.is_eaten_pink_ghost,
                                                                            self.is_eaten_player1,
                                                                            self.is_eaten_player2, self.wait_signal,
                                                                            self.tournament_num_of_players))

            self.process_multiplayer.daemon = True
            self.process_multiplayer.start()
            sleep(3)
            QGuiApplication.processEvents()
            while self.wait_signal.value == 1:
                print("waiting for player")
           # sleep(3)
           # QGuiApplication.processEvents()

        if self.network_id.value == 1:
            self.x_position_player1.value = self.player2_start_position[0] * 20
            self.y_position_player1.value = self.player2_start_position[1] * 20
            self.x_position_player2.value = self.player_start_position[0] * 20
            self.y_position_player2.value = self.player_start_position[1] * 20
            self.player1.position = [self.x_position_player1.value//20, self.y_position_player1.value//20]
            self.player2.position = [self.x_position_player2.value//20, self.y_position_player2.value//20]
            self.player1.draw(self.x_position_player1.value, self.y_position_player1.value,
                              self.stopped_moving_player1)
            self.player2.draw(self.x_position_player2.value, self.y_position_player2.value,
                              self.stopped_moving_player2)

        # start players

        self.player1_process.daemon = True
        self.player1_process.start()

        if self.number_of_players == 2:
            self.player2_process.daemon = True
            self.player2_process.start()

        # start ghosts processes
        if self.number_of_players in (1, 2):
            self.green_ghost_process.daemon = True
            self.green_ghost_process.start()
            self.red_ghost_process.daemon = True
            self.red_ghost_process.start()
            self.yellow_ghost_process.daemon = True
            self.yellow_ghost_process.start()
            self.pink_ghost_process.daemon = True
            self.pink_ghost_process.start()

            self.special_power_thread.daemon = True
            self.special_power_thread.start()
        else:
            if self.number_of_players == 3:
                if self.network_id.value == 0:
                    self.green_ghost_process.daemon = True
                    self.green_ghost_process.start()
                    self.red_ghost_process.daemon = True
                    self.red_ghost_process.start()
                    self.yellow_ghost_process.daemon = True
                    self.yellow_ghost_process.start()
                    self.pink_ghost_process.daemon = True
                    self.pink_ghost_process.start()

                    self.special_power_thread.daemon = True
                    self.special_power_thread.start()

        # sve dok se igirca ne iskljuci crtaj labele za pacmana, ghost na osnovu pozicije koju posalje proces
        while self.exit_queue.empty():

            if self.network_id.value == 1 and self.number_of_players == 3:
                d = self.is_eaten_player1.value
                self.is_eaten_player1.value = self.is_eaten_player2.value
                self.is_eaten_player2.value = d
                self.player1.lives = self.lives_player1.value
                self.player2.lives = self.lives_player2.value

            if self.lives_up_to_date:
                if self.number_of_players == 1:
                    if self.is_eaten_player1.value == 1:
                        self.lives -= 1
                        self.lives_up_to_date = False
                        if self.lives == 0:
                            self.game_over()
                            self.show_game_over_image()
                            self.game_is_over = True
                            while self.game_over_space_key == 0:
                                sleep(0.1)
                                QGuiApplication.processEvents()

                            self.kill_all_processes()
                            self.close()
                        else:
                            self.update_status_bar()
                            self.show_ready_image()
                elif self.number_of_players == 2:
                    if self.is_eaten_player1.value == 1 or self.is_eaten_player2.value == 1:
                        self.lives -= 1
                        self.lives_up_to_date = False
                        if self.lives == 0:
                            self.game_over()
                            self.show_game_over_image()
                            self.game_is_over = True
                            while self.game_over_space_key == 0:
                                sleep(0.1)
                                QGuiApplication.processEvents()

                            self.kill_all_processes()
                            self.close()
                        else:
                            self.update_status_bar()
                            self.show_ready_image()
                else:
                    if self.network_id.value == 0:
                        if self.is_eaten_player1.value == 1:
                            self.lives_player1.value -= 1
                            self.player1.lives = self.lives_player1.value
                            if self.lives_player1.value <= 0:
                                self.game_over()
                                self.show_game_over_image()
                                self.game_is_over = True
                                while self.game_over_space_key == 0:
                                    sleep(0.1)
                                    QGuiApplication.processEvents()

                                self.kill_all_processes()
                                self.close()
                            else:
                                self.update_status_bar()
                                self.show_ready_image()
                        if self.is_eaten_player2.value == 1:
                            self.lives_player2.value -= 1
                            self.player2.lives = self.lives_player2.value
                            if self.lives_player2.value <= 0:
                                self.game_over()
                                self.show_game_over_image()
                                self.game_is_over = True
                                while self.game_over_space_key == 0:
                                    sleep(0.1)
                                    QGuiApplication.processEvents()

                                self.kill_all_processes()
                                self.close()
                            else:
                                self.update_status_bar()
                                self.show_ready_image()

            if self.lives_up_to_date is False and self.number_of_players == 1:
                if self.is_eaten_player1.value == 0:
                    self.lives_up_to_date = True
                    self.hide_ready_image()
                    self.hide_game_over_image()
                    if self.lives == 0:
                        self.kill_all_processes()
                        self.close()

            if self.lives_up_to_date is False and self.number_of_players == 2:
                if self.is_eaten_player1.value == 0 and self.is_eaten_player2.value == 0:
                    self.lives_up_to_date = True
                    self.hide_ready_image()
                    self.hide_game_over_image()
                    if self.lives == 0:
                        self.kill_all_processes()
                        self.close()

            if self.lives_up_to_date is False and self.number_of_players == 3:
                if self.is_eaten_player1.value == 0 and self.is_eaten_player2.value == 0:
                    self.lives_up_to_date = True
                    self.hide_ready_image()
                    self.hide_game_over_image()
                    if self.lives_player1.value <= 0 or self.lives_player2.value <= 0:
                        self.kill_all_processes()
                        self.close()

            '''if (self.is_eaten_player1.value == 1 or self.is_eaten_player2.value == 1) and self.lives_up_to_date:
                if self.number_of_players == 3:
                    if self.network_id.value == 0:
                        if self.is_eaten_player1.value == 1:
                            self.lives_player1.value -= 1
                            self.player1.lives = self.lives_player1.value
                        else:
                            self.lives_player2.value -= 1
                            self.player2.lives = self.lives_player2.value
                else:
                    self.lives -= 1
                self.lives_up_to_date = False
                if self.lives_player1.value <= 0 or self.lives_player2.value <= 0 or self.lives == 0:
                    self.game_over()
                    self.show_game_over_image()
                    self.game_is_over = True

                    while self.game_over_space_key == 0:
                        sleep(0.1)
                        QGuiApplication.processEvents()

                    self.kill_all_processes()
                    self.close()
                else:
                    self.update_status_bar()
                    self.show_ready_image()
                    # self.hide_ready_image()
            elif (self.is_eaten_player1.value == 0 and self.is_eaten_player2.value == 0) and self.number_of_players == 3:
                self.lives_up_to_date = True
                self.hide_ready_image()
                self.hide_game_over_image()
                if self.lives_player1.value == 0 or self.lives_player2.value == 0:
                    self.kill_all_processes()
                    self.close()
            elif (self.is_eaten_player1.value == 0 and self.is_eaten_player2.value == 0) and self.number_of_players != 3:
                self.lives_up_to_date = True
                self.hide_ready_image()
                self.hide_game_over_image()
                if self.lives == 0:
                    self.kill_all_processes()
                    self.close()

                # prvo odraditi proveru koliko zivota koji pacman ima
                # ukoliko ima 0, znaci da se prikazuje game over
                # u suprotnom prikaz ready
                # trebalo bi ovde neki sleep da se odradi
            '''

            self.player1.head_position(self.head_direction_player1.value)
            x = int(self.x_position_player1.value // self.cell_width)
            y = int(self.y_position_player1.value // self.cell_height)
            self.player1.pick_up_coin(Vector(x, y), 1)
            self.player1.draw(self.x_position_player1.value, self.y_position_player1.value, self.stopped_moving_player1)

            # get special power position if multiplayer
            if self.number_of_players == 3:
                if self.network_id.value == 1:
                    if self.special_power_pixel_position_x.value == -1.0:
                        self.eat_ghosts_power_label.setHidden(True)
                        self.special_power_position = Vector(-5, -5)
                    else:
                        self.eat_ghosts_power_label.setGeometry(self.special_power_pixel_position_x.value,
                                                                self.special_power_pixel_position_y.value,
                                                                20, 20)
                        self.eat_ghosts_power_label.setHidden(False)
                        self.special_power_position = Vector(int(self.special_power_pixel_position_x.value // 20), int(self.special_power_pixel_position_y.value // 20))

            if self.number_of_players in (2, 3):

                self.player2.head_position(self.head_direction_player2.value)
                x = int(self.x_position_player2.value // self.cell_width)
                y = int(self.y_position_player2.value // self.cell_height)
                self.player2.pick_up_coin(Vector(x, y), 2)
                self.player2.draw(self.x_position_player2.value, self.y_position_player2.value,
                                  self.stopped_moving_player2)
                self.player2.lives = self.lives_player2.value
                self.player1.lives = self.lives_player1.value


            # draw gosts
            self.green_ghost.draw(self.x_position_green_ghost.value, self.y_position_green_ghost.value)
            self.red_ghost.draw(self.x_position_red_ghost.value, self.y_position_red_ghost.value)
            self.yellow_ghost.draw(self.x_position_yellow_ghost.value, self.y_position_yellow_ghost.value)
            self.pink_ghost.draw(self.x_position_pink_ghost.value, self.y_position_pink_ghost.value)

            self.update_ghosts_picture()

            if self.network_id.value == 0:
                if self.can_eat_ghost.value > 0:
                    self.can_eat_ghost.value -= 1
                else:
                    self.can_eat_ghost.value = 0

            '''if len(self.coins) == 1 and brzina == False:
                if [self.x_position_player1.value // 20, self.y_position_player1.value // 20] == [0, 14] or\
                   [self.x_position_player1.value // 20, self.y_position_player1.value // 20] == [27, 14]:
                    self.ghost_speed.value = 2.0
                    brzina = True'''

            self.level_up()

            '''if brzina is True:
                for x in self.start_coins:
                    self.coins.append(x)'''

            sleep(0.01)
            QGuiApplication.processEvents()

        self.kill_all_processes()
        self.close()

    def level_up(self):
        if len(self.coins) <= 1 and self.brzina is False:
            if [self.x_position_player1.value // 20, self.y_position_player1.value // 20] == [0, 14] or \
                    [self.x_position_player1.value // 20, self.y_position_player1.value // 20] == [27, 14]:
                self.ghost_speed.value = 2.0
                self.brzina = True
                for x in self.start_coins:
                    self.coins.append(x)


    def set_players(self):
        self.player1 = Player(self, self.player1_label, self.player_start_position)
        self.x_position_player1.value = self.player1.pixel_position.x
        self.y_position_player1.value = self.player1.pixel_position.y
        self.player1.draw(self.player1.pixel_position.x, self.player1.pixel_position.y, self.stopped_moving_player1)

        self.player1_process = Process(target=move_player,
                                       args=(self.x_position_player1, self.y_position_player1, self.player1.position,
                                             self.walls, self.key_queue, self.exit_queue, self.head_direction_player1,
                                             self.stopped_moving_player1, self.is_eaten_player1))

        if self.number_of_players in (2, 3):
            self.player2 = Player(self, self.player2_label, self.player2_start_position)
            self.x_position_player2.value = self.player2.pixel_position.x
            self.y_position_player2.value = self.player2.pixel_position.y
            self.player2.draw(self.player2.pixel_position.x, self.player2.pixel_position.y, self.stopped_moving_player2)
            if self.number_of_players == 2:
                self.player2_process = Process(target=move_player,
                                               args=(
                                                   self.x_position_player2, self.y_position_player2, self.player2.position,
                                                   self.walls, self.key_queue_player2, self.exit_queue,
                                                   self.head_direction_player2, self.stopped_moving_player2,
                                                   self.is_eaten_player2))

    def set_status_bars(self):
        font = QFont('SansSerif', 12)
        font.setBold(True)
        self.setWindowTitle("1 player mode")
        self.status_bar.setGeometry(0, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT,
                                    380, STATUS_BAR_HEIGHT)
        self.status_bar.setAutoFillBackground(True)
        self.status_bar.setObjectName('status_bar')
        self.status_bar.setFont(font)
        self.status_bar.setStyleSheet("QLabel#status_bar { background-color: black; color: white;}")

        self.high_score_bar.setGeometry(380, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT,
                                        180, STATUS_BAR_HEIGHT)
        self.high_score_bar.setAutoFillBackground(True)
        self.high_score_bar.setObjectName('high_score_bar')
        self.high_score_bar.setFont(font)
        self.high_score_bar.setStyleSheet("QLabel#high_score_bar { background-color: black; color: white;}")
        self.high_score_bar.setText("HIGH SCORE: {score}".format(score=self.high_score))

        if self.number_of_players != 3:
            self.status_bar.setText("PLAYER 1: {score}      LIVES: {lives}".format(score=self.player1.score, lives=self.lives))
        elif self.number_of_players == 3:
            self.status_bar.setText("YOU: {score}   LIVES: {lives}".format(score=self.player1.score, lives=self.player1.lives))

        if self.number_of_players in (2, 3):
            self.setWindowTitle("2 player mode")
            self.status_bar_player2.setAutoFillBackground(True)
            self.status_bar_player2.setObjectName('status_bar_2')
            self.status_bar_player2.setFont(QFont(font))
            self.status_bar_player2.setStyleSheet("QLabel#status_bar_2 { background-color: black; color: white;}")
            self.status_bar_player2.setGeometry(220, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT, 160, STATUS_BAR_HEIGHT)

            if self.number_of_players == 2:
                self.status_bar_player2.setText("   PLAYER 2: {secondScore}".format(
                    secondScore=self.player2.score))

            if self.number_of_players == 3:
                self.setWindowTitle("Multiplayer")

                self.status_bar_player2.setGeometry(170, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT, 200,
                                                    STATUS_BAR_HEIGHT)
                self.status_bar_player2.setText("PLAYER 2: {secondScore}   LIVES: {lives}".format(
                    secondScore=self.player2.score, lives=self.player2.lives))

    def set_ghosts(self):
        # green ghost setup
        self.green_ghost = Ghost(self, 1, self.ghosts_position[0], self.green_ghost_label, 1)
        self.green_ghost.draw(self.green_ghost.pixel_position.x, self.green_ghost.pixel_position.y)
        self.x_position_green_ghost.value = self.green_ghost.pixel_position.x
        self.y_position_green_ghost.value = self.green_ghost.pixel_position.y
        # set ghost mode
        self.green_ghost_mode.value = self.green_ghost.mode

        self.green_ghost_process = Process(target=do_ghost_mode,
                                           args=(self.x_position_green_ghost, self.y_position_green_ghost,
                                                 self.green_ghost.position,
                                                 self.green_ghost_mode, self.x_position_player1,
                                                 self.y_position_player1, self.walls, self.is_eaten_player1,
                                                 self.is_eaten_player2,
                                                 self.can_eat_ghost, self.is_eaten_green_ghost,
                                                 self.x_position_player2,
                                                 self.y_position_player2,
                                                 self.ghost_speed))

        # yellow ghost setup
        self.yellow_ghost = Ghost(self, 2, self.ghosts_position[1], self.yellow_ghost_label, 2)
        self.yellow_ghost.draw(self.yellow_ghost.pixel_position.x, self.yellow_ghost.pixel_position.y)
        self.x_position_yellow_ghost.value = self.yellow_ghost.pixel_position.x
        self.y_position_yellow_ghost.value = self.yellow_ghost.pixel_position.y
        # set ghost mode
        self.yellow_ghost_mode.value = self.yellow_ghost.mode
        self.yellow_ghost_process = Process(target=do_ghost_mode,
                                            args=(self.x_position_yellow_ghost, self.y_position_yellow_ghost,
                                                  self.yellow_ghost.position, self.yellow_ghost_mode,
                                                  self.x_position_player1, self.y_position_player1, self.walls,
                                                  self.is_eaten_player1, self.is_eaten_player2,
                                                  self.can_eat_ghost, self.is_eaten_yellow_ghost,
                                                  self.x_position_player2, self.y_position_player2,
                                                  self.ghost_speed))

        # red ghost setup
        self.red_ghost = Ghost(self, 1, self.ghosts_position[2], self.red_ghost_label, 0)
        self.red_ghost.draw(self.red_ghost.pixel_position.x, self.red_ghost.pixel_position.y)
        self.x_position_red_ghost.value = self.red_ghost.pixel_position.x
        self.y_position_red_ghost.value = self.red_ghost.pixel_position.y
        # set ghost mode
        self.red_ghost_mode.value = self.red_ghost.mode
        self.red_ghost_process = Process(target=do_ghost_mode, args=(self.x_position_red_ghost,
                                                                     self.y_position_red_ghost, self.red_ghost.position,
                                                                     self.red_ghost_mode, self.x_position_player1,
                                                                     self.y_position_player1, self.walls,
                                                                     self.is_eaten_player1, self.is_eaten_player2,
                                                                     self.can_eat_ghost,
                                                                     self.is_eaten_red_ghost,
                                                                     self.x_position_player2, self.y_position_player2,
                                                                     self.ghost_speed))

        # pink ghost setup
        self.pink_ghost = Ghost(self, 2, self.ghosts_position[3], self.pink_ghost_label, 0)
        self.pink_ghost.draw(self.pink_ghost.pixel_position.x, self.pink_ghost.pixel_position.y)
        self.x_position_pink_ghost.value = self.pink_ghost.pixel_position.x
        self.y_position_pink_ghost.value = self.pink_ghost.pixel_position.y
        # set ghost mode
        self.pink_ghost_mode.value = self.pink_ghost.mode
        self.pink_ghost_process = Process(target=do_ghost_mode, args=(self.x_position_pink_ghost,
                                                                      self.y_position_pink_ghost,
                                                                      self.pink_ghost.position,
                                                                      self.pink_ghost_mode, self.x_position_player1,
                                                                      self.y_position_player1, self.walls,
                                                                      self.is_eaten_player1, self.is_eaten_player2,
                                                                      self.can_eat_ghost,
                                                                      self.is_eaten_pink_ghost,
                                                                      self.x_position_player2, self.y_position_player2,
                                                                      self.ghost_speed))

    def center_window(self):
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2,
                  (screen.height() - size.height() - 40) / 2)

    def read_walls(self):
        with open("walls.txt", 'r') as file:
            for yidx, line in enumerate(file):
                for xidx, char in enumerate(line):
                    if char == "1":
                        self.walls.append(Vector(xidx,
                                                 yidx))  # svaku poziciju gde je 1 upisuje u walls, uzima x i y kooridinatu te pozicije
                    elif char == "B":
                        self.ghost_passage.append(Vector(xidx, yidx))  # prolaz kroz koji ghostovi izlaze
                    elif char == "C":
                        self.path_cells.append(Vector(xidx, yidx))
                        self.start_coins.append(Vector(xidx, yidx))
                        self.coins.append(
                            Vector(xidx, yidx))  # ako je na nekom mestu C to znaci da tu treba da se nalazi coin
                    elif char == "P":
                        self.player_start_position = [xidx, yidx]  # P je pozicija prvog igraca
                    elif char == "K":
                        self.player2_start_position = [xidx, yidx]  # K je pozicija drugog igraca ako se u dvoje igra

                        if self.number_of_players == 1:
                            self.coins.append(Vector(xidx, yidx))

                    elif char in ["2", "3", "4", "5"]:
                        self.ghosts_position.append([xidx, yidx])  # na mestu brojeva 12345 na mapi ce se naci ghost
                    elif char == "G":
                        self.eat_ghosts_power_positions.append(Vector(xidx, yidx))

    def update_status_bar(self):

        if self.number_of_players == 3:
            self.status_bar.setText(
                "YOU: {firstScore}  LIVES: {fistLives}".format(firstScore=self.player1.score,
                                                                     fistLives=self.player1.lives))
            self.status_bar_player2.setText("PLAYER 2: {secondScore} LIVES: {secondlives}".format(
                secondScore=self.player2.score, secondlives=self.player2.lives))
        else:
            self.status_bar.setText(
                "PLAYER 1: {firstScore}   LIVES: {fistLives}".format(firstScore=self.player1.score,
                                                                     fistLives=self.lives))
        if self.number_of_players == 2:
            self.status_bar_player2.setText("PLAYER 2: {secondScore}".format(
                secondScore=self.player2.score))

    def update_ghosts_picture(self):
        if self.can_eat_ghost.value > 0:
            if self.is_eaten_green_ghost.value == 1:
                self.green_ghost.image = QPixmap("images\\ghost_eyes.png").scaled(self.cell_height, self.cell_width)
            else:
                self.green_ghost.image = QPixmap("images\\scaredghost.jpg").scaled(self.cell_height, self.cell_width)
            if self.is_eaten_pink_ghost.value == 1:
                self.pink_ghost.image = QPixmap("images\\ghost_eyes.png").scaled(self.cell_height, self.cell_width)
            else:
                self.pink_ghost.image = QPixmap("images\\scaredghost.jpg").scaled(self.cell_height, self.cell_width)
            if self.is_eaten_red_ghost.value == 1:
                self.red_ghost.image = QPixmap("images\\ghost_eyes.png").scaled(self.cell_height, self.cell_width)
            else:
                self.red_ghost.image = QPixmap("images\\scaredghost.jpg").scaled(self.cell_height, self.cell_width)
            if self.is_eaten_yellow_ghost.value == 1:
                self.yellow_ghost.image = QPixmap("images\\ghost_eyes.png").scaled(self.cell_height, self.cell_width)
            else:
                self.yellow_ghost.image = QPixmap("images\\scaredghost.jpg").scaled(self.cell_height, self.cell_width)
        else:
            self.green_ghost.image = QPixmap("images\\green_ghost_img.jpg").scaled(self.cell_height,
                                                                                   self.cell_width)
            self.red_ghost.image = QPixmap("images\\green_ghost_img.jpg").scaled(self.cell_height,
                                                                                 self.cell_width)
            self.yellow_ghost.image = QPixmap("images\\yellow_ghost_img.png").scaled(self.cell_height,
                                                                                     self.cell_width)
            self.pink_ghost.image = QPixmap("images\\yellow_ghost_img.png").scaled(self.cell_height,
                                                                                   self.cell_width)

    def game_over(self):
        if self.high_score_changed:
            self.write_high_score(self.high_score)
        self.show_game_over_image()
        self.status_bar.setGeometry(0, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT,
                                    380, STATUS_BAR_HEIGHT)
        self.high_score_bar.setGeometry(380, PLAYING_WINDOW_HEIGHT - STATUS_BAR_HEIGHT,
                                        180, STATUS_BAR_HEIGHT)
        if self.number_of_players == 2:
            self.player1.score += self.player2.score
            self.status_bar_player2.setHidden(True)
        self.status_bar.setText(
            "     GAME OVER    YOUR SCORE: {firstScore}".format(firstScore=self.player1.score))

        if self.number_of_players == 3:
            player_name = ""
            player_score = 0

            if self.player1.score > self.player2.score:
                player_name = "YOU"
                player_score = self.player1.score
            else:
                player_name = "PLAYER2"
                player_score = self.player2.score

            self.status_bar.setText(
                "  WINNER: {player}   SCORE: {firstScore}".format(player=player_name, firstScore=self.player1.score))
        """self.kill_all_processes()
        self.close()"""

    def get_highest_score(self):
        scores = []
        highest_score = 0

        file = QFile("high_score_file.txt")

        if file.open(QFile.ReadOnly | QFile.Text):
            one_score = file.readLine()

            if one_score == b'':
                file.close()
                return
            if one_score != b'\n':
                scores.append(int(one_score))

            while one_score:
                one_score = file.readLine()
                if one_score != b'':
                    scores.append(int(one_score))
                else:
                    file.close()
        file.close()
        if len(scores) != 0:
            highest_score = max(scores)
        else:
            highest_score = 0
        return highest_score

    def write_high_score(self, score):
        f = open("high_score_file.txt", "a+")
        f.write(str(score) + "\n")
        f.close()

    def keyPressEvent(self, event):
        # ova fja se sama poziva nakon klika nekog dugmeta na tastaturi, kliknutu strelicu smestamo u queue iz kog ce proces da uzima smer
        if event.key() == Qt.Key_Down:
            self.key_queue.value = 1

        if event.key() == Qt.Key_Up:
            self.key_queue.value = 3

        if event.key() == Qt.Key_Left:
            self.key_queue.value = 4

        if event.key() == Qt.Key_Right:
            self.key_queue.value = 2

        if event.key() == Qt.Key_Escape:
            self.exit_queue.put(1)

        if event.key() == Qt.Key_Space and self.game_is_over is True:
            self.game_over_space_key = 1

        # player 2 event key
        if event.key() == Qt.Key_S:
            self.key_queue_player2.value = 1

        if event.key() == Qt.Key_W:
            self.key_queue_player2.value = 3

        if event.key() == Qt.Key_A:
            self.key_queue_player2.value = 4

        if event.key() == Qt.Key_D:
            self.key_queue_player2.value = 2

    def generate_random_power(self, show_time, hide_time):
        run = True

        while run:
            sleep(hide_time)
            self.show_special_power()
            sleep(show_time)
            self.hide_special_power()

    def hide_ready_image(self):
        self.player_ready.setHidden(True)

    def show_ready_image(self):
        self.player_ready.setHidden(False)
        """467 LINIJA (X,Y,SIRINA,VISINA)"""
        self.player_ready.setGeometry(190, 40, 800, 610)

    def hide_game_over_image(self):
        self.game_over_label.setHidden(True)

    def show_game_over_image(self):
        self.game_over_label.setHidden(False)
        self.game_over_label.setGeometry(190, 40, 800, 610)

    def get_random_special_power_position(self):
        index_position = random.randint(0, len(self.coins))

        if len(self.coins) > 1:
            if self.path_cells[index_position] in self.coins:
                self.was_a_coin = True
                return self.path_cells[index_position]
        else:
            return Vector(-2, -2)

    def show_special_power(self):
        self.eat_ghosts_power_label.setHidden(False)
        self.special_power_position = self.get_random_special_power_position()
        self.special_power_pixel_position_x.value = self.special_power_position.x * 20
        self.special_power_pixel_position_y.value = self.special_power_position.y * 20
        self.eat_ghosts_power_label.setGeometry(self.special_power_pixel_position_x.value,
                                                self.special_power_pixel_position_y.value,
                                                20, 20)

    def hide_special_power(self):
        self.was_a_coin = False
        self.eat_ghosts_power_label.setHidden(True)
        self.special_power_position = vec(-1, -1)
        self.special_power_pixel_position_x.value = -1
        self.special_power_pixel_position_y.value = -1

    def kill_all_processes(self):
        self.player1_process.kill()
        if self.number_of_players == 2:
            self.player2_process.kill()
        if self.number_of_players == 3:
            self.process_multiplayer.kill()
        self.green_ghost_process.kill()
        self.pink_ghost_process.kill()
        self.red_ghost_process.kill()
        self.yellow_ghost_process.kill()
