from PyQt5.QtGui import QPixmap
from settings import *
import pygame
from PyQt5.QtCore import QSize
import random
vec = pygame.math.Vector2


class Ghost(object):
    def __init__(self, app, color, start_position, label, mode):
        self.app = app
        self.start_position = start_position
        self.position = start_position
        self.pixel_position = self.get_pixel_position()
        self.direction = vec(0, 0)
        self.last_direction = vec(0, 0)
        self.label = label
        self.previous_direction = vec(0, 0)
        self.speed = 1
        self.get_path_thread = None

        if color == 1:
            self.image = QPixmap("images\\green_ghost_img.jpg").scaled(self.app.cell_height, self.app.cell_width)
        else:
            self.image = QPixmap("images\\yellow_ghost_img.png").scaled(self.app.cell_height, self.app.cell_width)

        self.wrong_direction = vec(0, 0)
        self.mode = mode
        self.path = None

    def get_pixel_position(self):
        return vec((self.position[0] * self.app.cell_width),
                   (self.position[1] * self.app.cell_height))

    def draw(self, x_position, y_position):
        self.pixel_position.x = x_position
        self.pixel_position.y = y_position
        self.label.setPixmap(self.image)
        self.label.setGeometry(self.pixel_position.x, self.pixel_position.y,
                               self.app.cell_width, self.app.cell_height)

